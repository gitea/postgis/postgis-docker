Docker image build instructions for PostGIS

See http://postgis.net

Note that in order to push to postgis-docker.osgeo.org, you must login with your osgeo account

via

> docker login postgis-docker.osgeo.org


For pulling images, it is best to use the group docker. Pulling can be done anoymously

> docker pull postgis-docker.osgeo.org/postgis/build-test:trisquel11


The included make file automates this the below example logs in,
builds and then pushes the trisquel11 image.

```sh
cd build-test
make login
make trisquel11
make trisquel11-push
```


[![Jenkins CI](https://debbie.postgis.net/job/PostGIS-docker/label=docker/badge/icon)](https://debbie.postgis.net/job/PostGIS-docker/)
